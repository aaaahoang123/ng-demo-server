import { Controller, Get, Post, Body } from '@nestjs/common';
import { AppService } from './app.service';
import { InjectRepository } from '@nestjs/typeorm';
import { Category } from './shared/entities/category.entity';
import { Repository } from 'typeorm';
import { Product } from './shared/entities/product.entity';
import { ProductReqDto } from './shared/req-dtos/product-req.dto';
import { CategoryReqDto } from './shared/req-dtos/category-req.dto';
import { CategoryResDto } from './shared/res-dtos/category-res.dto';
import { ProductResDto } from './shared/res-dtos/product-res.dto';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    @InjectRepository(Category) private readonly cateRepo: Repository<Category>,
    @InjectRepository(Product) private readonly productRepo: Repository<Product>,
  ) { }

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Post('categories')
  async createCategory(@Body() dto: CategoryReqDto) {
    const instance = new Category();
    const result = await this.cateRepo.save(Object.assign(instance, dto));
    return new CategoryResDto(result);
  }

  @Get('categories')
  async listCategories() {
    const categories = await this.cateRepo.find();
    return categories.map(category => new CategoryResDto(category));
  }

  @Post('products')
  async createProduct(@Body() dto: ProductReqDto) {
    const instance = new Product();
    const result = await this.productRepo.save(Object.assign(instance, dto));
    return new CategoryResDto(result);
  }

  @Get('products')
  async listProducts() {
    const products = await this.productRepo.find({
      relations: ['category'],
    });
    return products.map(product => new ProductResDto(product));
  }
}
