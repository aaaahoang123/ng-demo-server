import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { CustomValidationPipe } from './shared/pipes/custom-validation.pipe';
import { RestfulInterceptor } from './shared/interceptors/restful.interceptor';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(new CustomValidationPipe({ transform: true }));
  app.useGlobalInterceptors(new RestfulInterceptor());
  app.enableCors();
  await app.listen(3000);
}
bootstrap();
