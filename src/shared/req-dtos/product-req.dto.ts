import { IsNotEmpty, IsNumber } from 'class-validator';

// tslint:disable
export class ProductReqDto {
    @IsNotEmpty()
    name: string;
    @IsNotEmpty()
    @IsNumber()
    category_id: number;
}
