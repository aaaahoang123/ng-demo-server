import { Category } from '../entities/category.entity';
import { format } from 'date-fns';
import { CLIENT_DATE_FORMAT } from '../constant.resource';

// tslint:disable
export class CategoryResDto {
    id: number;
    name: string;
    created_at: number;
    updated_at: number;
    status: number;

    created_at_str: string;
    updated_at_str: string;

    constructor(input: Category) {
        Object.assign(this, input);
        this.created_at_str = format(new Date(input.created_at), CLIENT_DATE_FORMAT);
        this.updated_at_str = format(new Date(input.updated_at), CLIENT_DATE_FORMAT);
    }
}
