import { IsEmail, IsNotEmpty } from 'class-validator';
export class CategoryReqDto {
    @IsNotEmpty()
    name: string;
}
