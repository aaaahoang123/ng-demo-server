import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Category } from '../../shared/entities/category.entity';

@Module({
    imports: [
        TypeOrmModule.forRoot(),
        TypeOrmModule.forFeature([Category]),
    ],
    exports: [TypeOrmModule],
})
export class DatabaseModule { }
