import { Entity, PrimaryGeneratedColumn, Column, BeforeInsert, BeforeUpdate } from 'typeorm';

// tslint:disable
@Entity('categories')
export class Category {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ length: 255 })
    name: string;
    @Column({ type: 'bigint' })
    created_at: number;
    @Column({ type: 'bigint' })
    updated_at: number;
    @Column({ type: 'bigint', default: 1 })
    status: number;

    @BeforeInsert()
    bi() {
        const now = new Date().valueOf();
        this.created_at = now;
        this.updated_at = now;
        this.status = 1;
    }

    @BeforeUpdate()
    bu() {
        this.updated_at = new Date().valueOf();
    }
}
