import { Entity, PrimaryGeneratedColumn, Column, BeforeInsert, BeforeUpdate, JoinColumn, ManyToOne } from 'typeorm';
import { Category } from './category.entity';

// tslint:disable
@Entity('products')
export class Product {
    @PrimaryGeneratedColumn('increment')
    id: number;

    @Column({ length: 255 })
    name: string;

    @Column({ type: 'int' })
    category_id: number;

    @JoinColumn({ name: 'category_id', referencedColumnName: 'id' })
    @ManyToOne(type => Category)
    category: Category;

    @Column({ type: 'bigint' })
    created_at: number;

    @Column({ type: 'bigint' })
    updated_at: number;

    @Column({ type: 'bigint', default: 1 })
    status: number;

    @BeforeInsert()
    bi() {
        const now = new Date().valueOf();
        this.created_at = now;
        this.updated_at = now;
        this.status = 1;
    }

    @BeforeUpdate()
    bu() {
        this.updated_at = new Date().valueOf();
    }
}
