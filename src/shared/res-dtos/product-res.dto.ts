import { CategoryResDto } from './category-res.dto';
import { Product } from '../entities/product.entity';
import { format } from 'date-fns';
import { CLIENT_DATE_FORMAT } from '../constant.resource';

// tslint:disable
export class ProductResDto {
    id: number;

    name: string;

    category_id: number;
    category: CategoryResDto;

    created_at: number;

    updated_at: number;

    status: number;

    created_at_str: string;
    updated_at_str: string;

    constructor(input: Product) {
        this.id = input.id;
        this.name = input.name;
        this.category_id = input.category_id;
        this.created_at = input.created_at;
        this.updated_at = input.updated_at;
        this.status = input.status;
        this.created_at_str = format(new Date(input.created_at), CLIENT_DATE_FORMAT);
        this.updated_at_str = format(new Date(input.updated_at), CLIENT_DATE_FORMAT);
        this.category = input.category && new CategoryResDto(input.category);
    }
}
