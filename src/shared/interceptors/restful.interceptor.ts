import { CallHandler, ExecutionContext, Injectable, NestInterceptor, Logger } from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class RestfulInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    const excludes = [];
    const req = context.switchToHttp().getRequest();
    const url = req.url;
    return next
      .handle()
      .pipe(
        map(data => {
          for (const exclude of excludes) {
            if (url.includes(exclude)) {
              return data;
            }
          }
          const result: any = {
            status: 1,
            message: 'Thành công',
          };
          if (!data) {
            return { ...result, data };
          }
          const { datas, meta } = data;
          if (datas) {
            return {
              ...result,
              datas,
              meta,
            };
          }
          if (Array.isArray(data)) {
            return { ...result, datas: data };
          }
          return {
            ...result,
            data,
          };
        }),
      );
  }
}
